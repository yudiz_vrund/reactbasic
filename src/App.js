import React from 'react';
import FunctionEx from './FunctionEx';
import ClassEx from './ClassEx';
import StateEx from './StateEx';
import StateExWithClass from './StateExWithClass';
import PropExample from './PropExample';
import LifeCycleEx from './LifeCycleEx';
import UnmountEx from './UnmountEx';
import GetDerivedEx from './GetDerivedEx';
import ShouldComponentEx from './ShouldComponentEx';
import UseEffectEx from './UseEffectEx';

const App = () => {
    return(
        <>
        <div>
             <h4>Normal Function Example</h4>
            <FunctionEx name={"Vrund"} ></FunctionEx>
            <h4>Normal Class Example</h4>
            <ClassEx/>
            <h4>Normal Function with state and decrement counter Example</h4>
            <StateEx/>
            <h4>Normal Class with state and increment counter Example</h4>
            <StateExWithClass/>
            <h4>Checking Props Example</h4>
            <PropExample />
            <h4>Example Of life cycle</h4> 
            <LifeCycleEx/>
             <h4>Unmount Example</h4>
            <UnmountEx/>
            <h4>Derived Example</h4>
            <GetDerivedEx/>
            <h4>Should Component Example</h4>
            <ShouldComponentEx/>
            <h4>Use Effect example</h4>
            <UseEffectEx/> 
        </div>
        </>
    )
}

export default App;
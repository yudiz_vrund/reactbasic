import React, { Component } from 'react';

class LifeCycleEx extends Component {
    constructor()
    {
        super();
        console.log("In constructor..!!");
        this.state = {
            count : 1
        }
    }

    componentWillMount()
    {
        console.log("In Will mount..!!");
    }

    componentDidMount()
    {
        this.setState({count:2});
        console.log("In Component did mount..!!");
    }

    componentDidUpdate()
    {
        console.log("In update mode!!!");
    }

    render(){
        console.log("In render method!!");
        return(
            <div>
                <h1>{this.state.count}</h1>
            </div>
        )
    }
}

export default LifeCycleEx;
import React, { useState } from 'react';

const StateEx = () => {

    const [oldCounter, newCounter] = useState(100);

    function decrease() {
        if(oldCounter > 0){
            newCounter(oldCounter - 1);
        }
        else
        {
            alert("no less value than 0 ");
        }
        
    }

    return (<div><h2>{oldCounter}</h2><button onClick={decrease}>Decrement </button></div>)

}

export default StateEx;
import React, { useEffect, useState } from 'react';

const UseEffectEx = () => {

    const [oldnum, newnum] = useState(1);
    const [olddigit, newdigit] = useState(1);

    useEffect(()=>{
        alert("click!!");
    },[oldnum])

    return (<div>
        <button onClick={() => { newnum(oldnum + 1) }}>Click me {oldnum}</button>
        <button onClick={() => { newdigit(olddigit + 1) }}>click me {olddigit}</button>
    </div>)
}


export default UseEffectEx;
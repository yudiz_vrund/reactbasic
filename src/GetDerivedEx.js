import React, { Component } from 'react';


class GetDerivedEx extends Component{
    constructor()
    {
        super();
        this.state = {
            counter : 1
        }
    }
    render()
    {
        return(
            <div>
                <h1>base class counter is {this.state.counter}</h1>
                <Child data={this.state.counter}/>
                <button onClick={() => {this.setState({counter: this.state.counter +1})}}> + </button>          
            </div>
        )
    }
}

class Child extends Component{
    constructor(){
        super();
        this.state = {
            currentValue : 1
        }
        
    }

    static getDerivedStateFromProps(props,state)
    {
        console.log("In derived method");
        console.log(state);
        return {
            currentValue : props.data * 10

        }
    }

    render()
    {
        console.log("render method called");
        return(
            <div>
                <h1>Child counter is {this.state.currentValue}</h1>
            </div>
        )
    }
}

export default GetDerivedEx;
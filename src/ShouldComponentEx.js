import React, { Component } from 'react';


class ShouldComponentEx extends Component {
    constructor()
    {
        super();
        this.state = {
            counter: 1
        }
    }


    shouldComponentUpdate(prevProp,prevState)
    {   
        console.log(prevState.counter,this.state.counter)
        if(prevState.counter === this.state.counter)
        {
            return false
        }
     return true   
    }

    render() {
        console.log("in Render!!!");
        return (
        <div>
        <h1>counter {this.state.counter}</h1>
        <button onClick={() => {this.setState({counter:this.state.counter+1})}}>update</button>
        </div>
        )
    }
}

export default ShouldComponentEx;
import React, { Component } from 'react';

class ClassEx extends Component {
    constructor()
    {
        super();
        this.state={
            name:"Vrund Shah"
        }
    }   
    render() {
        return (<div><h2>Hiii {this.state.name}</h2></div>)
    }
}

export default ClassEx;
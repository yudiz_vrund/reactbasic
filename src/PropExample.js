import React from 'react';
import PropTypes from 'prop-types';

const PropExample = (props) => {

    return(
        <>
        <h2>My name is {props.name}</h2>
        </>
    )
}


PropExample.propTypes = {
    name: PropTypes.string   //for required name: PropTypes.string.isRequired 
     //when you input the string then it's ok other wise show the warning
}

PropExample.defaultProps = {
    name : "default vrund!!!!"
}


export default PropExample;


import React, { Component } from 'react';

class UnmountEx extends Component {
    constructor()
    {
        super();
        this.state = {
            show : false
        }
    }
    render(){
        return(
            <div>
                
                {this.state.show ? <Child/> : null}
                <button onClick={()=> {this.setState({show: !this.state.show})}}>Unmount button</button>
            </div>
        )
    }
}


class Child extends Component{

    componentWillUnmount()
    {

        console.log("Unmount the component!!!");
    }

    render(){
        return(
            <div>
                <h3>In child Class</h3>
            </div>
        )
    }
}


export default UnmountEx;